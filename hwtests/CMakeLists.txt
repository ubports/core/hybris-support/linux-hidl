add_executable(hwtest_ping hwtest_ping.cpp)
target_link_libraries(hwtest_ping libutils libcutils android.hidl.base_1.0 android.hidl.manager_1.0)
add_executable(hwtest_vibrator hwtest_vibrator.cpp)
target_link_libraries(hwtest_vibrator libutils libcutils android.hidl.base_1.0 android.hidl.manager_1.0 android.hardware.vibrator_1.0)

install(TARGETS hwtest_ping hwtest_vibrator
        RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
        PUBLIC_HEADER)
