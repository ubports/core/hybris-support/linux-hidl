#!/bin/bash

set -e

MB=$1

OLD_WD=`pwd`
cd patches

MBS=$(find . -name *.patch -exec dirname {} \; |sort -u)
for mb in $MBS; do
    cd $OLD_WD/src/$mb
    git am $OLD_WD/patches/$mb/*.patch
done

cp -r $OLD_WD/cmake/* $OLD_WD/src/
