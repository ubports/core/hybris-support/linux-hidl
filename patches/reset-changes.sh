#!/bin/bash

set -e

MB=$1

OLD_WD=`pwd`
cd patches

MBS=$(find . -name *.patch -exec dirname {} \; |sort -u)
for mb in $MBS; do
    cd $OLD_WD/src/$mb
    BRANCH=`git rev-parse --abbrev-ref HEAD`
    git reset --hard origin/$BRANCH
    git clean -f
done
