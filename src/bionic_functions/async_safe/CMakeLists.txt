add_library(async_safe STATIC
    async_safe_log.cpp
)

target_include_directories(async_safe PUBLIC include)
target_include_directories(async_safe PRIVATE ../include ..)

install(DIRECTORY include/async_safe DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/linux-hidl)
